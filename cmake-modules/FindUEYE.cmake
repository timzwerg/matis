

if (UEYE_LIBRARIES AND UEYE_INCLUDE_DIRS)
    # UEYE befindet sich bereits im cache
    set(UEYE_FOUND TRUE)
else (UEYE_LIBRARIES AND UEYE_INCLUDE_DIRS)
    set(UEYE_DEFINITIONS "")

    find_path(UEYE_INCLUDE_DIR
            NAMES
            ./ueye.h
            PATHS
            /usr/include
            /usr/local/include
            /opt/local/include
            /sw/include
            )

    find_library(UEYE_LIBRARY
            NAMES
            ueye_api
            PATHS
            /usr/lib
            /usr/local/lib
            /opt/local/lib
            /usr/local/share
            /sw/lib
            )

    if (UEYE_LIBRARY)
        set(UEYE_FOUND TRUE)
    endif (UEYE_LIBRARY)

    set(UEYE_INCLUDE_DIRS
            ${UEYE_INCLUDE_DIR}
            )

    if (UEYE_FOUND)
        set(UEYE_LIBRARIES
                ${UEYE_LIBRARIES}
                ${UEYE_LIBRARY}
                )
    endif (UEYE_FOUND)

    if (UEYE_INCLUDE_DIRS AND UEYE_LIBRARIES)
        set(UEYE_FOUND TRUE)
    endif (UEYE_INCLUDE_DIRS AND UEYE_LIBRARIES)

    if (UEYE_FIND_REQUIRED AND NOT UEYE_FOUND)
        message(FATAL_ERROR "Could not find ueye")
    endif (UEYE_FIND_REQUIRED AND NOT UEYE_FOUND)


endif (UEYE_LIBRARIES AND UEYE_INCLUDE_DIRS)