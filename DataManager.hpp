
#ifndef MATIS_DATAMANAGER_H
#define MATIS_DATAMANAGER_H



#include <string>
#include "json.hpp"
#include <fstream>
#include "def.hpp"
#include <ctime>
#include <iostream>




using json = nlohmann::json;
using namespace std;



class DataManager {

public:
    DataManager();
    ~DataManager();

    //Materialverwaltung
    void loadDataFromFile();
    void saveDataToFile();
    void addMaterial(json material);
    bool removeMaterial(int imageId);
    int getNewId();
    void clearData();
    string dumpDB();

    json getMaterialForPastecId(int imageId);
    json getMaterialForMaterialNr(int materialNr);

    //Einstellungen
    const char *getPastecSocket();
    string getUnknownPartId();

    const char *getTCPPort();

    void loadSettings();
    //Logging
    void logEvent(EventStruct eventStruct);
    string getLog();

private:
    json settings;
    json materials;

    string getCurrentTimeStamp();
};


#endif //MATIS_DATAMANAGER_H
