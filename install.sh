#!/bin/bash
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
BUILDPATH=$SCRIPTPATH/build
echo "$BUILDPATH"
rm -r "$BUILDPATH"
mkdir "$BUILDPATH"
cd "$BUILDPATH"
cmake ../
make
if [ -e "$1" ]; then
    ./MatIS "$1"
else
    ./MatIS
fi
