
#include <thread>
#include "Server.hpp"

int main(int argc, char * argv[]) {

    if (argc <= 1) {
        printf("IDS ueye Bilderkennungssoftware:\n"
                       "Bedienung: MatIS <StartModus> <ip> <port>\n"
                        "StartModi:\n"
                       "\t-t\tStandard-Teile der Datenbank hinzufügen und starten\n"
                       "\t-r\tnormaler Start\n"
                       "-a\t<imageFile> <materialName> <materialNr>\n"
//                       "\tTODO:\n"
//                       "\t-i\tIP:Port von Pastec-Server vorgeben\n" //Kann in config eingestellt werden --> muss hier nicht mehr implementiert werden
//                       "\t-l\tstatt -i: pastec auf localhost:4212\n"
//                       "-d\t<materialNr>\n"
        );
        return 0;
    }


//    std::cout << "There are " << argc << " arguments:\n";
//    for (int count=0; count < argc; ++count)
//        std::cout << count << " " << argv[count] << '\n';

    Server *server = new Server();
    int serverReturnValue;
    if(!strcmp(argv[1], "-t")) {
        server->prepareForTesting();
        serverReturnValue = server->run();
        return serverReturnValue;
    }else if (!strcmp(argv[1], "-r")){
        serverReturnValue = server->run();
        return serverReturnValue;
    } else if (!strcmp(argv[1], "-a")){
        if (argc<5){
            printf("-a <Bild-Dateipfad> <MaterialName> <MaterialNr>: Neue Trainings-Referenz anlegen\n"
                           "\tBild-Dateipfad z.B.: \"/home/pi/neu.jpg\"\n"
                           "\t(Alternativ: \"-\" angeben, damit neues Bild aufgenommen wird.)\n");
        }else{
            const char* imageFile = argv[2];
            const char* materialName = argv[3];
            int materialNr = atoi(argv[4]);
            bool success = server->addMaterial(materialName, imageFile, materialNr);
            if (success){
                printf("Bild erfolgreich hinzugefügt.\n");
                return 0;
            }
            return 1;
        }
    }


//    HTTPServer *httpServer = new HTTPServer();
//    MyController myPage;

//    WebServer wServer(8080);
//    wServer.addController(&myPage);
//    wServer.start();
//    t1.join();

}
