#ifndef PASTECCONNECTOR_HPP_
#define PASTECCONNECTOR_HPP_

#include <stdio.h>
#include <curl/curl.h>
#include <string>
#include <sys/stat.h>
#include <fcntl.h>
#include "json.hpp"
#include <cstdlib>
#include "def.hpp"

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>



class RESTConnector {
    using json = nlohmann::json;

public:
	std::string pastecSocket;

	RESTConnector(const char *pastecSocketParam);
    ~RESTConnector();

    bool addImage(int imageId, const char *file);
    bool removeImage(int imageId);
    SearchResponseStruct searchImage(const char *file);
    bool ping();
	bool sendToMiddleware(int materialNr, std::string auftragsNummer);
private:
	CURL *curl = 0;


    int pastecPID = 0;
	enum PastecMethod{search, add, remove, pingM};

    json performRESTRequest(int imageId, const char *filePath, PastecMethod method);

	const char *concatURL(const char *theURL, int param);
};

#endif