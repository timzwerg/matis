#include "StaticCamera.hpp"

StaticCamera::StaticCamera(){
	INT nRet = is_InitCamera (&hCam, NULL);

  //Pixel-Clock setzen
	UINT nPixelClockDefault = 9;
	nRet = is_PixelClock(hCam, IS_PIXELCLOCK_CMD_SET,
		(void*)&nPixelClockDefault,
		sizeof(nPixelClockDefault));

  //Farbmodus der Kamera setzen
	INT colorMode = IS_CM_BGR8_PACKED;
	nRet = is_SetColorMode(hCam,colorMode);


//	double *actualExposure = 0.0;
//	double desiredExposureInMs =  500.0;
//	nRet = is_SetExposureTime(hCam, desiredExposureInMs, actualExposure);
//	printf("Bildaufnahme> Gewünschte Aufnahmezeit: %f, tatsächliche: %f", desiredExposureInMs, &actualExposure);

    //Bildgroesse einstellen -> 1280x1024
//    UINT formatID = 25;

//	Bildgroesse einstellen -> 800x600
    UINT formatID = 29;

	nRet = is_ImageFormat(hCam, IMGFRMT_CMD_SET_FORMAT, &formatID, 4);

	//Speicher fuer Bild alloziieren
	char* pMem = NULL;
	int memID = 0;
	nRet = is_AllocImageMem(hCam, 800, 600, 24, &pMem, &memID);

  //diesen Speicher aktiv setzen
	nRet = is_SetImageMem(hCam, pMem, memID);

  //Bilder im Kameraspeicher belassen
	INT displayMode = IS_SET_DM_DIB;
	nRet = is_SetDisplayMode (hCam, displayMode);

}

int StaticCamera::captureImage(){
	//Bild aufnehmen
	INT nRet = is_FreezeVideo(hCam, IS_WAIT);
  //Bild aus dem Speicher auslesen und als Datei speichern
	IMAGE_FILE_PARAMS ImageFileParams;

	ImageFileParams.pwchFileName = L"./lastImg.jpg";
	ImageFileParams.pnImageID = NULL;
	ImageFileParams.ppcImageMem = NULL;
	ImageFileParams.nQuality = 0;
	ImageFileParams.nFileType = IS_IMG_JPG;

	nRet = is_ImageFile(hCam, IS_IMAGE_FILE_CMD_SAVE, (void*) &ImageFileParams, sizeof(ImageFileParams));
	printf("Bildaufnahme mit Status %d beendet.\n", nRet);
    return nRet;

}

StaticCamera::~StaticCamera(){
	is_ExitCamera(hCam);
}