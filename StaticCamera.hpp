#ifndef STATICCAMERA_HPP_
#define  STATICCAMERA_HPP_


#include <stdio.h>
#include <stddef.h>
#include <ueye.h>

class StaticCamera
{
public:
	StaticCamera();
	~StaticCamera();
	int captureImage();
private:
	HIDS hCam = (HIDS) 0;

};

#endif 