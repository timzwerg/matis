#! /bin/sh
### BEGIN INIT INFO
# Provides:          MatIS
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Startet und Beendet den Bilderkennungs-Server als
# Description:       Starts & Stops Speye
### END INIT INFO
#Achtung: Bitte auf den / am Ende des Pfades achten (sollte vorhanden sein)
CPP_MATIS_DIR="/home/pi/StudienarbeitBilderkennung/matis/build/"

PASTEC_NAME="pastec"
PASTEC_DIR="/home/pi/StudienarbeitBilderkennung/pastec/build/"
PASTEC_VISUAL_WORDS_FILENAME="visualWordsORB.dat"
#auf false setzen, wenn Pastec auf einem anderen Host läuft und hier nicht mitgestartet werden soll
PASTEC_RUNS_LOCALLY=true

NODE_NAME="server.js"
NODE_CAMERA_SERVER_DIR="/home/pi/StudienarbeitBilderkennung/nodejsSpeye/server/"
NODE_EXE_DIR="/usr/local/bin/node"

MATIS_NAME="MatIS"

#PIDFILES: Enthalten die process IDs der entsprechenden Prozesse (für das eindeutige Beenden der Prozesse benötigt)
PIDDIR="~/.pidfiles/"
CPP_PIDFILE=${PIDDIR}${MATIS_NAME}.pid
NODE_PIDFILE=${PIDDIR}${NODE_NAME}.pid
PASTEC_PIDFILE=${PIDDIR}${PASTEC_NAME}.pid

if [ ! -d "${PIDDIR}" ];then
    mkdir "${PIDDIR}"
    echo "~/.pidfiles Verzeichnis angelegt"
fi

if [ "$1" = "status" ];then
    if [ -e "${CPP_PIDFILE}" ]; then
        echo "c++ MatIS ist derzeit aktiv"
    fi
    if [ -e "${PASTEC_PIDFILE}" ]; then
        echo "Pastec ist derzeit aktiv"
    fi
    if [ -e "${NODE_PIDFILE}" ]; then
        echo "Der NodeJS-BilderkennungsServer ist derzeit aktiv"
    fi

    exit 0
fi

case "$2" in
    c++) #--> Es wird die C++ Variante der Bilderkennung gestartet/verwaltet
    case "$1" in
        start)
        if [ -e "${NODE_PIDFILE}" ]; then
            echo "NodeJS Server ist noch aktiv. Bitte diesen zuerst stoppen."
            exit 1
        fi
     #Aktion wenn start c++ uebergeben wird
            echo "Starte c++ ${MATIS_NAME}"
            if [ "${PASTEC_RUNS_LOCALLY}" = true ]; then
                echo "Starte Pastec"
                start-stop-daemon --start --make-pidfile --pidfile "${PASTEC_PIDFILE}" --remove-pidfile  --exec "${PASTEC_DIR}${PASTEC_NAME}" -- "${PASTEC_DIR}${PASTEC_VISUAL_WORDS_FILENAME}" &
                echo "Pastec wurde mit Signal $? gestartet"
            fi

#            Start CPP-Server here
            start-stop-daemon --start --chdir "${CPP_MATIS_DIR}" --make-pidfile --pidfile "${CPP_PIDFILE}" --remove-pidfile  --exec "${CPP_MATIS_DIR}${MATIS_NAME}" -- "-r"
            ;;

        stop)
     #Aktion wenn stop uebergeben wird
            echo "Stoppe c++ ${MATIS_NAME}"
            #Stoppe Pastec wenn es lokal gestartet wurde
            if [ "$PASTEC_RUNS_LOCALLY" = true ]; then
                start-stop-daemon --remove-pidfile --stop --pidfile "${PASTEC_PIDFILE}"
                echo "Pastec wurde mit Signal $? gestoppt"
            fi

            start-stop-daemon --remove-pidfile --stop --pidfile "${CPP_PIDFILE}"
            RETVAL="$?"
            echo "${MATIS_NAME} mit Signal ${RETVAL} beendet."
            return "$RETVAL"
            ;;
    esac
    ;;

    node)
    case "$1" in
        start)
        if [ -e "${CPP_PIDFILE}" ]; then
            echo "c++ MatIS ist noch aktiv. Bitte dieses zuerst stoppen."
            return 1;
        fi
     #Aktion wenn start uebergeben wird
            echo "Starte nodeJS Server"
            start-stop-daemon --start --chdir "${NODE_CAMERA_SERVER_DIR}" --make-pidfile --pidfile "${NODE_PIDFILE}" --remove-pidfile  --exec "${NODE_EXE_DIR}" -- "${NODE_CAMERA_SERVER_DIR}${NODE_NAME}"
            RETVAL="$?"
            echo "Der nodeJS Server wurde mit Signal ${RETVAL} gestartet."
            return "$RETVAL"
            ;;

        stop)
     #Aktion wenn stop uebergeben wird
            echo "Stoppe nodeJS Server"
            start-stop-daemon --remove-pidfile --stop --pidfile "${NODE_PIDFILE}"
            RETVAL="$?"
            echo "Der nodeJS Server wurde mit Signal ${RETVAL} beendet."
            return "$RETVAL"
            ;;

    esac
    ;;
    *)
     echo -e "Bedienung des Ueye-Bilderkennungsservice:
     (start|stop|status) (node|c++)
     Beispiel:
     \tmatisd.sh start c++   startet die C++-Variante der Bilderkennungssoftware
     \tmatisd.sh status      zeigt die derzeit aktive Materialerkennungs-Software an"
     ;;
esac

exit 0


