
#ifndef DEF_HPP_
#define DEF_HPP_

#include <string>
#include <limits.h>
#include <unistd.h>
//Dateinamen
static const char* SETTINGS_FILE_NAME = "Einstellungen.json";
static const char* MATERIAL_FILE_NAME = "MaterialListe.json";
static const char *const LOG_FILE_NAME = "Log.txt";

//JSON: Material
static const char *const MATERIAL_NR = "materialNr";
static const char *const MATERIAL_NAME = "materialName";
static const char *const PASTEC_ID = "id";

static const char *const PASTEC_IP = "pastecIP";
static const char *const PASTEC_PORT = "pastecPort";
static const char *const TCP_PORT = "tcpPort";

static const char *const UNKNOWN_MATERIAL_ID = "UnknownMaterialNr";

using namespace std;
using json = nlohmann::json;

typedef unsigned int uint;

struct MaterialStruct{
    int id;
    int objektNr;
    std::string objektName;
};

struct MaterialReturnStruct{
    MaterialStruct ts;
    int auftragsNr;
    double score;
};

enum EventType{searchEvent, additionEvent, removalEvent, pingEvent, errorEvent};

struct EventStruct{
    EventType eventType;
    string materialName;
    string materialNr;
    string auftragsNummer;
    string score;
    string errorText;
};

struct SearchResponseStruct{
    json idsArray;
    json scoresArray;
};

static std::string getexepath()
{
    char result[ PATH_MAX ];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
    return std::string( result, (count > 0) ? count : 0 );
}

#endif /* DEF_HPP_ */
