#!/bin/bash
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
IPFILE="${SCRIPTPATH}/pastecIP.txt"
IP=`cat $IPFILE`
echo "Server IP: $IP"

case "$1" in
	-p)
	echo "ping"
	curl -X POST -d '{"type":"PING"}'  http://${IP}:4212/ # --libcurl delmeCURL.txt
	;;

	-a)
	echo "add"
	curl -X PUT --data-binary @"$2" http://${IP}:4212/index/images/"$3"
	;;

	-d)
	echo "delete"
	curl -X DELETE http://${IP}:4212/index/images/"$2"
	;;

	-s)
	echo "searching"
	START=$(gdate +%s.%N)
	curl -X POST --data-binary @"$2" http://${IP}:4212/index/searcher
	END=$(gdate +%s.%N)
	DIFF=$(echo "$END - $START" | bc)
	echo "Time elapsed: $DIFF"
	;;

	-c)
	echo "$2" > $IPFILE
	echo "New IP: $2"
	;;

	*)
	echo -e "Usage: $0 {-a|-d|-s|-p|-c}\nadd(file, id)|delete(id)|search(file)|ping|config(ip)"
	;;
esac
