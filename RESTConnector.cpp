

#include <signal.h>
#include "RESTConnector.hpp"
#include <exception>

using json = nlohmann::json;

struct MemoryStruct {
    char *memory;
    size_t size;
};

struct NoMatchFoundException : public std::exception {
    const char * what () const throw () {
        return "Es konnte kein passendes Material gefunden werden.\n";
    }
};



RESTConnector::RESTConnector(const char* pastecSocketParam){
    std::string tmp(pastecSocketParam);
    pastecSocket = tmp;

    while(!this->ping()){
        sleep(4);
        printf("Versuche Verbindung zum Pastec-Server auf %s aufzubinden.\n", pastecSocket.c_str() );
    }

    curl_global_init(CURL_GLOBAL_ALL);

    curl = curl_easy_init();
    if (!curl){
        fprintf(stderr, "Curl konnte nicht initialisiert werden.\n");
    }
    printf("RESTConnector wurde initialisiert: %s\n", pastecSocket.c_str());

}

RESTConnector::~RESTConnector(){
    curl_easy_cleanup(curl);
    curl_global_cleanup();
//    printf("Killing Pastec\n");
//    kill(pastecPID, SIGTERM);

}


const char* RESTConnector::concatURL(const char* theURL = "", int imageId = -1) {
//    const char* fuck = pastecSocket;
//    std::string tld(pastecSocket);

//    printf("PS: %s\n", pastecSocket);
    std::string res;
    if (imageId != -1) {
        res = pastecSocket + theURL + std::to_string(imageId);
    } else{
        res = pastecSocket+ theURL;
    }
    const char *rescc = res.c_str();
    return rescc;

}

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp){
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    mem->memory = (char *) realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory==NULL){
        printf("nicht genügend Speicher\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

static size_t ReadCallback(void *pointer, size_t size, size_t nmemb, void *stream)
{
    size_t returnCode;
    curl_off_t nread;

    returnCode = fread(pointer, size, nmemb, (FILE *) stream);

    nread = (curl_off_t)returnCode;

//    fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T " bytes from file\n", nread);

    return returnCode;
}

json RESTConnector::performRESTRequest(int imageId, const char *filePath, PastecMethod method)
{
    //CURL easy initialisieren
    curl = curl_easy_init();

    //Datei einlesen
    struct stat file_info;
    FILE * hd_src;

    //(JSON)Response Reader vorbereiten
    struct MemoryStruct chunk;
    chunk.memory = (char *) malloc(1);
    chunk.size = 0;

    //Empfang der JSON Daten vorbereiten
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &chunk);


    if(method == search || method == add) {

        //DateiUpload vorbereiten (wird bei remove nicht benötigt)

        //file_info wird für die dateigröße im HTTP Request benötigt
        stat(filePath, &file_info);
        curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t) file_info.st_size);

        //Callback Funktion
        curl_easy_setopt(curl, CURLOPT_READFUNCTION, ReadCallback);

        if(!(hd_src = fopen(filePath, "rb"))){
            fprintf(stderr, "Datei %s nicht gefunden.\n", filePath);
            json empty= {{"type","notfound"}};
            return empty;
        }

        //Datei zum Upload spezifizieren
        curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);

        //Upload aktivieren
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

    }


    //Useragent setzen --> kompabilitätsprobleme vermeiden
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    //URL setzen
    const char* finalURL = "";

    switch(method){
        case search:
            finalURL = concatURL("index/searcher");
            //HTTP Request Type setzen
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case add:
            finalURL = concatURL("index/images/", imageId);
            //HTTP Request Type setzen
            curl_easy_setopt(curl, CURLOPT_PUT, 1L);
            break;
        case remove:
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            finalURL = concatURL("index/images/", imageId);
            //HTTP Request Type setzen
//            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "DELETE"); --> führt zu fehlender JSON response
            break;
        case pingM:
            finalURL = concatURL();
            //HTTP Request Type setzen
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "{\"type\":\"PING\"}");
            curl_easy_setopt(curl, CURLOPT_POST, 1L);
            break;
    };
    curl_easy_setopt(curl, CURLOPT_URL, finalURL);
//    printf("Final URL: %s\n", finalURL);

    //Zeigt kompletten HTTP request/response an
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    //Request durchführen:
    CURLcode res = curl_easy_perform(curl);

    if (res != CURLE_OK && method != remove)
    {
        fprintf(stderr, "Pastec Aufruf fehlgeschlagen (%d): %s\n", method, curl_easy_strerror(res));
        return nullptr;
    }

//    printf("%lu bytes received\n", (long) chunk.size);
    std::string responseString = chunk.memory;
    json responseJSON = json::parse(responseString);


    //Cleanup
    curl_easy_cleanup(curl);

    if(method == add || method == search){
        free(chunk.memory);
        fclose(hd_src);
    }

//    std::string jsonDump = responseJSON.dump();
//    fprintf(stdout, "JSONResponse: %s\n", jsonDump.c_str());

    if (responseJSON.empty()){
        fprintf(stderr, "Leerer Rückgabewert vom Pastec Server\n");
    }

    return responseJSON;

}


bool RESTConnector::addImage(int imageId, const char *file) {
    json j = performRESTRequest(imageId, file, add);
    std::string successType = j.value("type", "-");
    if(!successType.compare("IMAGE_ADDED")){
        printf("Bild %s (ID: %d) erfolgreich zu Pastec hinzugefügt.\n", file, imageId);
        return true;
    } else{
        return false;
    }
}

//noch nicht implementiert da JSON in Request nötig
bool RESTConnector::ping() {
    json j = performRESTRequest(0, "", pingM);
    if (j.empty()){
        fprintf(stderr,"Pastec Server nicht verfügbar.\n");
        return false;
    }
    std::string successType = j.value("type", "-");
    if (!successType.compare("PONG")) {
        return true;
    } else {
        fprintf(stderr,"Ping unsuccessful\n");
        return false;
    }
}

bool RESTConnector::removeImage(int imageId) {
    json j = performRESTRequest(imageId, (char *) "empty", remove);
    std::string successType = j.value("type", "-");
    if(!successType.compare("IMAGE_REMOVED")){
        printf("Bild mit ID %d erfolgreich aus Pastec entfernt.\n", imageId);
        return true;
    } else{
        return false;
    }
}

SearchResponseStruct RESTConnector::searchImage(const char* file){
    json j = performRESTRequest(0, file, search);
    SearchResponseStruct responseStruct;
    if(j.count("image_ids") == 0 || j.count("scores") == 0){
        //Wenn keine Results gefunden wurden
        printf("Es wurden keine entsprechenden Bilder gefunden.\n");
//        throw NoMatchFoundException();
        return responseStruct;
    }
    responseStruct.idsArray = j.at("image_ids");
    responseStruct.scoresArray = j.at("scores");

    return responseStruct;
}

bool RESTConnector::sendToMiddleware(int materialNr, std::string auftragsNummer) {
    std::string dialogString = "http://10.50.12.131:18081/api/?DLG=ANR.UPDATE|ANR.ANR=" + auftragsNummer + "0030|USR=2400|ANR.ATYP=AG|ANR.TABLE=A|ANR.FU:51=201;" + std::to_string(materialNr) + "|";
    const char* dialogChar = dialogString.c_str();
    CURLcode res;
    curl = curl_easy_init();
    if(curl) {
        printf("Schicke Auftragsnummer %s, MaterialNr %d an Middleware.\n", auftragsNummer.c_str(), materialNr);
        curl_easy_setopt(curl, CURLOPT_URL, dialogChar);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 2L); //CURL Timeout: 2 Sekunden
        res = curl_easy_perform(curl);
        /* always cleanup */
        curl_easy_cleanup(curl);
        if (res != CURLE_OK){
            fprintf(stderr, "Middleware Request failed: %s\n", curl_easy_strerror(res));
            return false;
        }
        return true;
    }
    return false;
}

