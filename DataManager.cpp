
#include <deque>
#include "DataManager.hpp"



using namespace std;


DataManager::DataManager() {
    loadSettings();
    loadDataFromFile();
    printf("DataManager wurde initialisiert: %s\n", materials.dump().c_str());

}
DataManager::~DataManager() {
//    teile = nullptr;
}

void DataManager::loadSettings() {
    std::ifstream i(SETTINGS_FILE_NAME);
    if(i.is_open()){
        i >> settings;
        i.close();
    }else{
        //wenn Datei noch nicht vorhanden: Datei anlegen und mit Standarteinstellungen laden
        i.close();
        settings = {
                {UNKNOWN_MATERIAL_ID, "26"},
                {PASTEC_IP,           "localhost"},
                {PASTEC_PORT,         "4212"},
                {TCP_PORT,            "5432"},
        };
        std::ofstream o(SETTINGS_FILE_NAME);
        o << std::setw(4) << settings << std::endl;
        o.close();
        printf("%s wurden angelegt.\n", SETTINGS_FILE_NAME);
    }
}

string DataManager::getUnknownPartId(){
    std::string ip = settings.value(UNKNOWN_MATERIAL_ID, "26");
    return ip;
}


const char* DataManager::getPastecSocket(){
    std::string ip = settings.value(PASTEC_IP, "localhost");
    std::string port = settings.value(PASTEC_PORT, "4212");
    std::string temp = "http://" + ip + ":" + port + "/";
    const char* socket = temp.c_str();
    return socket;
}

const char *DataManager::getTCPPort() {
    const char* tcpport = settings.value(TCP_PORT, "5432").c_str();
    return tcpport;
}


void DataManager::loadDataFromFile() {
    std::ifstream i(MATERIAL_FILE_NAME);
    if(i.is_open()){
        i >> materials;
        i.close();
    }else{
        //wenn Datei noch nicht vorhanden
        i.close();
        std::ofstream o(MATERIAL_FILE_NAME);
        o << "[]\n";
        o.close();
        printf("%s wurde angelegt.\n", MATERIAL_FILE_NAME);
    }
}

void DataManager::saveDataToFile() {
    std::ofstream o(MATERIAL_FILE_NAME);
    o << std::setw(4) << materials << std::endl;
    o.close();
}

int DataManager::getNewId() {
    int prevId = 0;
    for (json::iterator it = materials.begin(); it != materials.end(); ++it) {
        json material = *it;
        int id = material.value("id", 0);
        if(id != prevId + 1){
            fprintf(stdout, "Möglicher Fehler in %s: ids nicht sequentiell", MATERIAL_FILE_NAME);
        }
        prevId = id;
    }
    return prevId + 1;
}

void DataManager::addMaterial(json material) {
    materials.push_back(material);
    this->saveDataToFile();
    std::string materialName = material.value(MATERIAL_NAME, "<Fehler mit Material>");
    printf("Material %s gespeichert.\n", materialName.c_str());
}

json DataManager::getMaterialForPastecId(int imageId) {
    for (json::iterator it = materials.begin(); it != materials.end(); ++it) {
        json material = *it;
        int id = material.value(PASTEC_ID, 0);
        if(id == imageId){
            return material;
        }
    }
    fprintf(stderr, "Material mit ImageID %d nicht in MaterialListe gefunden.\n", imageId);
    return 0;
}

json DataManager::getMaterialForMaterialNr(int materialNr) {
    for (json::iterator it = materials.begin(); it != materials.end(); ++it) {
        json material = *it;
        int id = material.value(MATERIAL_NR, 0);
        if(id == materialNr){
            return material;
        }
    }
    fprintf(stderr, "Material mit MaterialNummer %d nicht in MaterialListe gefunden.\n", materialNr);
    return 0;
}

bool DataManager::removeMaterial(int imageId) {
    int idx=0;
    for (json::iterator it = materials.begin(); it != materials.end(); ++it) {
        json currentMat = *it;
        int id = currentMat.value("id", -1);
        if (id == imageId){
            materials.erase(materials.begin() + idx);
            saveDataToFile();
            return true;
        }
        idx++;
    }
    return false;
}

string DataManager::dumpDB() {
    return materials.dump(4);
}

void DataManager::clearData() {
    materials = {};
    saveDataToFile();
    printf("%s wurde gelöscht.\n", MATERIAL_FILE_NAME);
}

void DataManager::logEvent(EventStruct eventStruct) {
    string logString = getCurrentTimeStamp();
    switch(eventStruct.eventType){
        case searchEvent:
            logString += ": Material Identifiziert: " + eventStruct.materialName + "; ID: " + eventStruct.materialNr + "; Score: " + eventStruct.score + " ;Auftragsnummer: " + eventStruct.auftragsNummer;
            break;
        case additionEvent:
            logString += ": Material zur DB hinzugefuegt: " + eventStruct.materialName + "; ID: " + eventStruct.materialNr;
            break;
        case removalEvent:
            logString += ": Material aus DB gelöscht: " + eventStruct.materialName + "; ID: " + eventStruct.materialNr;
            break;
        case pingEvent:
            logString += ": Ping erhalten";
            break;
        case errorEvent:
            logString += ": ERROR: " + eventStruct.errorText;
    }

    std::ofstream outfile;

    outfile.open(LOG_FILE_NAME, std::ios_base::app);
    outfile << logString << endl;
    outfile.close();

}

string DataManager::getLog() {
    string outString;
    ifstream infile;
    infile.open(LOG_FILE_NAME);
    if (infile.is_open()) {
//        infile.seekg(-1, ios_base::end);
        deque<string> lines;

        for ( std::string line; lines.size() < 10 && getline(infile,line); )
        {
            lines.push_back(line);
        }

        for ( std::string line; getline(infile,line); ) {
            lines.pop_front();
            lines.push_back(line);
        }

        for ( std::size_t i = 0; i < lines.size(); ++i )
        {
            outString += lines[i] + "\n";
        }


    }
    return outString;
}

string DataManager::getCurrentTimeStamp(){
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%d-%m-%Y %I:%M:%S",timeinfo);
    return string(buffer);

}


