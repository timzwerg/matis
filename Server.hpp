
#ifndef MATIS_SERVER_H
#define MATIS_SERVER_H



#include "RESTConnector.hpp"
#include "DataManager.hpp"

#include "json.hpp"
#include <stdio.h>
#include <string.h>
#include <string>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <arpa/inet.h>
#ifdef __arm__
#include "StaticCamera.hpp"
#endif
#include <iostream>


using json = nlohmann::json;

class Server {
    struct addrinfo hints, * res;

public:
    Server();
    ~Server();

    int run();

    void prepareForTesting();

    bool addMaterial(const char *materialName, const char *imageFile, int materialNr);
    bool deleteMaterial(int materialNummer);
private:
    int status;
    bool keepRunning;
    int listener;
    int new_conn_fd;
    RESTConnector *rc;
    DataManager *dm;
#ifdef __arm__
    StaticCamera *cam;

#endif
    const char* captureImageFilePath;
    void *getInternetAddr(sockaddr *sa);

    void handleRequest(string auftragsNummer);

    void handleTrainCommand(vector<string> command);

    void handlePingCommand();

    void handleForgetCommand(vector<string> command);

    void handleListCommand();

    void handleLogCommand();
};


#endif //MATIS_SERVER_H
