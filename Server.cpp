#include <thread>
#include "Server.hpp"

Server::Server() {
#ifdef __arm__
    cam = new StaticCamera();
#endif
    dm = new DataManager();
    rc = new RESTConnector(dm->getPastecSocket());
}

Server::~Server() {
    free(dm);
    free(rc);
#ifdef __arm__
    free(cam);
#endif
}

void Server::prepareForTesting(){

    dm->clearData();

    printf("Bereite Testdaten vor.\n");
    addMaterial("Leerer Träger", "../testbilder/000.nothing.1.jpg", 0);
    addMaterial("Palette", "../testbilder/025.palette.jpg", 25);
    addMaterial("Unterteil mit Platine", "../testbilder/060.unterteilMitPlatine.1.jpg", 60);
    addMaterial("ObererTeil_Raw", "../testbilder/100.upper_part_raw.1.jpg", 100);
    addMaterial("Pressed Part", "../testbilder/600.PressedPart.1.jpg", 600);
    addMaterial("Taschenlampe auf 3D-Druck", "../testbilder/700.Taschenlampe.1.jpg", 700);
    addMaterial("Taschenlampe auf Aluminium", "../testbilder/701.TaschenlampeAluminium.1.jpg", 701);

}

int Server::run() {
    // Before using hint you have to make sure that the data structure is empty
    memset(&hints, 0, sizeof hints);
    // Set the attribute for hint
    hints.ai_family = AF_UNSPEC; // We don't care V4 AF_INET or 6 AF_INET6
    hints.ai_socktype = SOCK_STREAM; // TCP Socket SOCK_DGRAM
    hints.ai_flags = AI_PASSIVE;

    // Fill the res data structure and make sure that the results make sense.
    const char* port = strdup(dm->getTCPPort());
    status = getaddrinfo(NULL, port, &hints, &res);
    if (status != 0) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    }

    // Create Socket and check if error occured afterwards
    listener = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (listener < 0) {
        fprintf(stderr, "socket error: %s\n", gai_strerror(status));
    }

    // Bind the socket to the address of my local machine and port number
    status = ::bind(listener, res->ai_addr, res->ai_addrlen);
    if (status < 0) {
        fprintf(stderr, "bind: %s\n", gai_strerror(status));
        EventStruct event;
        event.eventType = errorEvent;
        event.errorText = "Der TCP-Port konnte nicht gebunden werden. Evtl. befindet sich noch eine andere Instanz des Programms darauf.";
        dm->logEvent(event);
        return 1;
    }

    status = listen(listener, 10);
    if (status < 0) {
        fprintf(stderr, "listen: %s\n", gai_strerror(status));
    }

    // Free the res linked list after we are done with it
    freeaddrinfo(res);


    // We should wait now for a connection to accept

    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char s[INET6_ADDRSTRLEN]; // Ein leerer String

    // Größe der Datenstruktur berechnen
    addr_size = sizeof client_addr;

    printf("Warte auf TCP Verbindungen (Port: %s) ...\n", strdup(dm->getTCPPort()));
    keepRunning = true;
    while (keepRunning) {
        // Accept a new connection and return back the socket desciptor
        new_conn_fd = accept(listener, (struct sockaddr *) &client_addr, &addr_size);
        if (new_conn_fd < 0) {
            fprintf(stderr, "accept: %s\n", gai_strerror(new_conn_fd));
            continue;
        }

        inet_ntop(client_addr.ss_family, getInternetAddr((struct sockaddr *) &client_addr), s, sizeof s);
        printf("Verbunden mit %s \n", s);
        char buffer[256];
        bzero(buffer, 256);
        int n = read(new_conn_fd, buffer, 255);

        this->handleRequest(buffer);

        close(new_conn_fd);
    }
    return 0;
}

vector<string> splitString(string theString){
    vector<string> vec(3);
    int i = 0;
    stringstream ssin(theString);
    while (ssin.good() && i < 3){
        ssin >> vec[i];
        ++i;
    }
    return vec;
}

void Server::handleTrainCommand(vector<string> command){
    string feedback;
    if (command[1] == "" || command[2] == ""){
        feedback = "Bedienung: TRAIN <MaterialNr> <MaterialName>";
    } else {
        int materialNr = atoi(command[1].c_str());
        string materialName = command[2];
        const char *imageName = "-";
        bool success = this->addMaterial(materialName.c_str(), imageName, materialNr);
        if (success) {
            feedback = "Material " + materialName + " wurde mit der ID " + to_string(materialNr) +
                       " der Datenbank hinzugefügt.\n";
        } else {
            feedback = "Material " + materialName + " konnte nicht hinzugefügt werden.";
        }
    }
    status = send(new_conn_fd, feedback.c_str(), feedback.length(), 0);
}

void Server::handlePingCommand(){
    string feedback = "Pong";
    EventStruct event;
    event.eventType = pingEvent;
    dm->logEvent(event);
    status = send(new_conn_fd, feedback.c_str(), feedback.length(), 0);
}

void Server::handleForgetCommand(vector<string> command){
    string feedback;
    if (command[1] == ""){
        feedback = "Bedienung: DELETE <MaterialNr>";
    } else {
        int materialNr = atoi(command[1].c_str());
        bool success = this->deleteMaterial(materialNr);
        if (success) {
            feedback = "Material mit der ID " + to_string(materialNr) +
                       " wurde aus der Datenbank gelöscht.\n";
        } else {
            feedback = "Material mit der ID " + to_string(materialNr) + " konnte nicht gelöscht werden. Eventuell ist es bereits gelöscht.";
        }
    }
    status = send(new_conn_fd, feedback.c_str(), feedback.length(), 0);
}

void Server::handleListCommand(){
    string feedback = dm->dumpDB();
    status = send(new_conn_fd, feedback.c_str(), feedback.length(), 0);
}

void Server::handleLogCommand() {
    string feedback = dm->getLog();
    status = send(new_conn_fd, feedback.c_str(), feedback.length(), 0);
}

void Server::handleRequest(string auftragsNummer){

    //Lösche Newline-Character An Anfang und Ende der Auftragsnummer
    auftragsNummer.erase(remove(auftragsNummer.begin(),auftragsNummer.end(),'\n'),auftragsNummer.end());
    //Lösche potentielles Plus (+) am Anfang der Auftragsnummer
    string plus = "+";
    if (auftragsNummer.substr(0, 1) == plus){
        auftragsNummer.erase(0,1);
        printf("Plus am Anfang gelöscht.\n");
    }

    string trainCommand = "TRAIN";
    string pingCommand = "PING";
    string forgetCommand = "DELETE";
    string listCommand = "LIST";
    string logCommand = "LOG";
    vector<string> vec = splitString(auftragsNummer);
    string command = vec[0];
    //command zu UPPERCASE transformieren
    std::transform(command.begin(), command.end(), command.begin(), ::toupper);

    if (command == trainCommand){
        handleTrainCommand(vec);
        return;
    } else if (command == pingCommand){
        handlePingCommand();
        return;
    } else if (command == forgetCommand){
        handleForgetCommand(vec);
        return;
    } else if (command == listCommand){
        handleListCommand();
        return;
    } else if (command == logCommand){
        handleLogCommand();
        return;
    }


    printf("Auftragsnummer: %s\n", auftragsNummer.c_str());



//Kamera Aufnahme
#ifdef __arm__
  cam->captureImage();
#endif
//Bild an RESTConnector --> Bildanalyse; SearchResultStruct wird returned
    SearchResponseStruct searchResult = rc->searchImage("lastImg.jpg");
    if (searchResult.idsArray.empty() || searchResult.scoresArray.empty()){ //Wenn kein Ergebnis gefunden wurde oder ein Fehler auftrat wird die in den Einstellungen gespeicherte Unbekanntes-Material-ID zurückgegeben
        const char* nothingFound= dm->getUnknownPartId().c_str();
        rc->sendToMiddleware(atoi(nothingFound), auftragsNummer);
        status = send(new_conn_fd, nothingFound, strlen(nothingFound) , 0);
        return;
    }

    int mostLikelyId = searchResult.idsArray.at(0).get<int>();
    int mostLikelyScore = searchResult.scoresArray.at(0).get<int>();
    printf("%d\n",mostLikelyId);
    json material = dm->getMaterialForPastecId(mostLikelyId);
//    trs.auftragsNr = std::stoi(auftragsNummer);
//    trs.score = mostLikelyScore;
//
//    const char * oName = (material.at("objektName").get<std::string>() + "\n").c_str();
//    status = send(new_conn_fd, oName , strlen(oName) , 0);

    int materialNr = material.at(MATERIAL_NR).get<int>();

    //Log Such-Event
    EventStruct event;
    event.eventType = searchEvent;
    event.score = to_string(mostLikelyScore);
    event.materialName = material.value(MATERIAL_NAME, "<MaterialName nicht gefunden");
    event.materialNr = to_string(materialNr);
    event.auftragsNummer = auftragsNummer;
    dm->logEvent(event);

//    Benachrichtigung an Middleware schicken
    rc->sendToMiddleware(materialNr, auftragsNummer);

    // Material-ID als TCP-Response schicken
    const char * oTID = to_string(materialNr).c_str();
    status = send(new_conn_fd, oTID , strlen(oTID) , 0);
//    const char *an = auftragsNummer.c_str();
//    status = send(new_conn_fd, an, strlen(an) , 0);

}

bool Server::addMaterial(const char *materialName, const char *imageFile, int materialNr){
    int imageId = dm->getNewId();
    if (!strcmp(imageFile, "-")){ //Wie in der Bedienungsanleitung beschrieben: Wenn - als BildPfad-Parameter angegeben wird, soll ein neues Bild aufgenommen werden
#ifdef __arm__
        int success = cam->captureImage();
        if (success != 0){
            EventStruct event;
            event.eventType = errorEvent;
            event.errorText = "Fehler beim Speichern der Bild-Datei: " + to_string(success);
            dm->logEvent(event);
        }
#endif
        imageFile="lastImg.jpg";
    }
    //Erst wenn Bild erfolgreich zu Pastec hinzugefügt wurde auch dem DataManager hinzufügen
    if (rc->addImage(imageId, imageFile)){
        json newMaterial = {{PASTEC_ID,    imageId},
                      {      MATERIAL_NR,   materialNr},
                      {      MATERIAL_NAME, materialName}
        };
        dm->addMaterial(newMaterial);

        //Log
        EventStruct addEvent;
        addEvent.eventType = additionEvent;
        addEvent.materialNr = to_string(materialNr);
        addEvent.materialName = materialName;
        dm->logEvent(addEvent);
        return true;
    }
    fprintf(stderr, "Material %s konnte nicht hinzugefügt werden.\n", materialName);
    return false;
}

bool Server::deleteMaterial(int materialNummer){
    json material = dm->getMaterialForMaterialNr(materialNummer);
    int imageId = -1;
    if (material.count(PASTEC_ID) > 0) {
        imageId = material.value(PASTEC_ID, -1);
    }
    if(imageId == -1) return false;
    if (rc->removeImage(imageId)){
        dm->removeMaterial(imageId);

        EventStruct event;
        event.eventType = removalEvent;
        event.materialNr = to_string(materialNummer);
        event.materialName = material.value(MATERIAL_NAME, "<Name nicht gefunden>");
        dm->logEvent(event);
        return true;
    }
    fprintf(stderr, "Material mit ImageID %d konnte nicht gelöscht werden.\n", materialNummer);
    return false;
}

void * Server::getInternetAddr(struct sockaddr *sa)
{
    if(sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

